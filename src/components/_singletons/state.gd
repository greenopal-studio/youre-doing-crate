extends Node


const MAX_ORDERS_ON_SCREEN = 5


# Mutable, shared state data:
var _order_list = []
var _orders_on_screen = []
var _current_level = ID.LEVEL_WAREHOUSE
var _points = 0
var _order_identification_number = 0
var _is_tutorial = false


func _ready():
	pass


func generate_order_list(entry_amount: int, available_items, max_items: int = 4):
	if _is_tutorial:
		set_order_list_for_tutorial()
		return

	for entry_no in entry_amount:
		var new_entry = {
			'items': [],
			'package_size': null,
			'is_priority': false,
			'is_fragile': false,
			'is_working_on': -1,
			'is_finished': false,
			'is_delivered': false,
			'order_identification_number': _order_identification_number
		}
		var item_amount = UTIL.rng.randi() % max_items
		var size = 0

		if UTIL.rng.randf() < 0.2:
			new_entry.is_priority = true
		for item_no in item_amount:
			if available_items.size() == 0:
				break
			
			var new_item = available_items[UTIL.rng.randi() % available_items.size()]
			var item_data = DATA.ITEMS[new_item]
			if size + item_data.size > DATA.PACKAGE_SIZES[ID.PACKAGE_SIZE_CRATE].space:
				if new_entry.items.size() > 0:
					break
				else:
					available_items.erase(new_item)
					continue
			else:
				new_entry.items.append(new_item)
				if 'is_fragile' in item_data:
					new_entry.is_fragile = true
				size += item_data.size
		for package_size in DATA.PACKAGE_SIZES.keys():
			if DATA.PACKAGE_SIZES[package_size].space >= size:
				new_entry.package_size = package_size
				break
		
		if new_entry.items.size() > 0:
			_order_list.append(new_entry)
			_order_identification_number += 1


func add_new_order_to_screen():
	var found_item = false
	for order_id in _order_list.size():
		if not order_id in _orders_on_screen and not is_order_finished(order_id):
			_orders_on_screen.append(order_id)
			EVENTS.emit_signal('new_order_appeared', order_id)
			found_item = true
			break
	if not found_item and _orders_on_screen.size() == 0 and not _is_tutorial:
		SCENE.goto_scene('win')


func set_order_list_for_tutorial():
	_order_list = [
		{
			'items': [ID.ITEM_RUBBER_DUCK],
			'package_size': ID.PACKAGE_SIZE_PAPERBAG,
			'is_priority': false,
			'is_fragile': false,
			'is_working_on': -1,
			'is_finished': false,
			'is_delivered': false,
			'order_identification_number': 1
		},
		{
			'items': [ID.ITEM_CONTROLLER],
			'package_size': ID.PACKAGE_SIZE_PACKAGE,
			'is_priority': false,
			'is_fragile': true,
			'is_working_on': -1,
			'is_finished': false,
			'is_delivered': false,
			'order_identification_number': 2
		},
		{
			'items': [ID.ITEM_RUBBER_DUCK, ID.ITEM_RUBBER_DUCK, ID.ITEM_RUBBER_DUCK, ID.ITEM_RUBBER_DUCK],
			'package_size': ID.PACKAGE_SIZE_CRATE,
			'is_priority': true,
			'is_fragile': false,
			'is_working_on': -1,
			'is_finished': false,
			'is_delivered': false,
			'order_identification_number': 3
		},
		{
			'items': [ID.ITEM_CONTROLLER, ID.ITEM_RUBBER_DUCK],
			'package_size': ID.PACKAGE_SIZE_PACKAGE,
			'is_priority': true,
			'is_fragile': true,
			'is_working_on': -1,
			'is_finished': false,
			'is_delivered': false,
			'order_identification_number': 4
		}
	]


func remove_order_from_screen(order_id: int):
	var index = _orders_on_screen.find(order_id)
	EVENTS.emit_signal('order_removed', index, order_id)
	_orders_on_screen.erase(order_id)
	add_new_order_to_screen()


func get_order(order_id: int) -> Dictionary:
	return _order_list[order_id]


func start_work_on_order(order_id: int, player_id: int):
	var order = _order_list[order_id]
	order.is_working_on = player_id


func is_order_being_worked_on_by(order_id: int, player_id: int):
	return _order_list[order_id].is_working_on == player_id and not is_order_finished(order_id)


func is_order_available(order_id: int):
	return _order_list[order_id].is_working_on == -1 and not _order_list[order_id].is_finished


func set_order_as_finished(order_id: int):
	var order = _order_list[order_id]
	order.is_finished = true
	EVENTS.emit_signal('order_finished', order_id)


func set_order_as_delivered(order_id: int):
	var order = _order_list[order_id]
	order.is_delivered = true
	remove_order_from_screen(order_id)


func is_order_finished(order_id: int):
	return _order_list[order_id].is_finished


func get_active_order_by_items(items: Array, is_fragile: bool = false) -> int:
	for screen_order_id in _orders_on_screen:
		var items_copy = items.duplicate()
		var order_data = _order_list[screen_order_id]
		if order_data.is_fragile != is_fragile:
			continue
		var order_items_copy = order_data.items.duplicate()
		for order_item in order_items_copy:
			items_copy.erase(order_item)
		if items_copy.size() == 0 and items.size() == order_items_copy.size():
			return screen_order_id
	return -1


func set_level(level_id: String):
	_current_level = level_id


func get_level() -> String:
	return _current_level


func add_points(amount: int):
	_points += amount
	EVENTS.emit_signal('points_changed', _points)


func get_points() -> int:
	return _points

func set_as_tutorial():
	_is_tutorial = true

func get_is_tutorial() -> bool:
	return _is_tutorial


func reset_level_data():
	_order_list = []
	_current_level = ID.LEVEL_WAREHOUSE
	_orders_on_screen = []
	_points = 0
	_is_tutorial = false
