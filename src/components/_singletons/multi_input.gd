extends Node

const DEADZONE = 0.1
const MOVE_FACTOR = 0.12
const DASH_MOVE_FACTOR = 0.18

const WASD_DEVICE_ID = -1
const ARROWS_DEVICE_ID = -2
const IJKL_DEVICE_ID = -3
const NUMPAD_DEVICE_ID = -4


var _current_players = []
var _is_join_open = true


signal players_changed(new_current_players)
signal joy_lost(id)
signal joy_reconnected(id)


func get_current_players():
	return _current_players

func get_current_players_count():
	return _current_players.size()

func get_current_player_colors() -> Array:
	var colors = []
	for player in _current_players:
		colors.append(player.color)
	return colors

func get_player_by_id(id: int):
	return _current_players[id]

func add_player(player):
	_current_players.append(player)

	emit_signal('players_changed', _current_players)

func update_player(id: int, color: Color):
	_current_players[id].color = color

	emit_signal('players_changed', _current_players)

func remove_player(id: int):
	_current_players.remove(id)

	emit_signal('players_changed', _current_players)

func reset():
	_current_players = []
	
	emit_signal('players_changed', [])

	_is_join_open = true

func disable_join():
	_is_join_open = false


func get_device_name_for_player(id):
	var device = _current_players[id].device

	if device == WASD_DEVICE_ID:
		return 'WASD Keys'
	elif device == ARROWS_DEVICE_ID:
		return 'Arrow Keys'
	elif device == IJKL_DEVICE_ID:
		return 'IJKL Keys'
	elif device == NUMPAD_DEVICE_ID:
		return 'Num Pad Keys'

	return Input.get_joy_name(device).replace('XInput', 'Xbox').replace('Standard Gamepad Mapping', 'Gamepad')


func get_move_vector_for_player(id: int, is_dash_active: bool):
	var device = _current_players[id].device

	var vector = Vector3.ZERO
	if device >= 0:
		var x = Input.get_joy_axis(device, JOY_AXIS_0)
		var z = Input.get_joy_axis(device, JOY_AXIS_1)
		vector = Vector3(x if abs(x) > 0.1 else 0.0, 0, z if abs(z) > 0.1 else 0.0)
	elif device == WASD_DEVICE_ID:
		vector = __get_move_vector_for_keyboard('wasd')
	elif device == ARROWS_DEVICE_ID:
		vector = __get_move_vector_for_keyboard('arrows')
	elif device == IJKL_DEVICE_ID:
		vector = __get_move_vector_for_keyboard('ijkl')
	elif device == NUMPAD_DEVICE_ID:
		vector = __get_move_vector_for_keyboard('numpad')

	if is_dash_active:
		vector *= DASH_MOVE_FACTOR
		return vector.limit_length(DASH_MOVE_FACTOR)
	else:
		vector *= MOVE_FACTOR
		return vector.limit_length(MOVE_FACTOR)



func get_hold_pressed_for_player(event, id: int):
	return __is_action_pressed_for_device(event, _current_players[id].device, 'hold')

func get_action_pressed_for_player(event, id: int):
	return __is_action_pressed_for_device(event, _current_players[id].device, 'action')

func get_powerup_pressed_for_player(event, id: int):
	return __is_action_pressed_for_device(event, _current_players[id].device, 'powerup')

func get_hold_released_for_player(event, id: int):
	return __is_action_released_for_device(event, _current_players[id].device, 'hold')

func get_action_released_for_player(event, id: int):
	return __is_action_released_for_device(event, _current_players[id].device, 'action')

func get_powerup_released_for_player(event, id: int):
	return __is_action_released_for_device(event, _current_players[id].device, 'powerup')


func _init():
	Input.connect('joy_connection_changed', self, '_on_joy_connection_changed')

func _on_joy_connection_changed(device: int, connected: bool):
	if connected:
		for i in _current_players.size():
			var player = _current_players[i]
			if player.device == device:
				emit_signal('joy_reconnected', device)
				print('[MultiInput] joy re-connected, is bound to player %d, device=%d'%[i, device])
				break
		print('[MultiInput] joy connected, device=%d "%s"'%[
			device, Input.get_joy_name(device)
		])
	else:
		for i in _current_players.size():
			var player = _current_players[i]
			if player.device == device:
				emit_signal('joy_lost', device)
				print('[MultiInput] joy lost, was bound to player %d, device=%d'%[i, device])
				break
		print('[MultiInput] joy lost, was not bound to a player, device=%d'%device)

func _input(event):
	if !_is_join_open:
		return

	var device
	if event is InputEventJoypadButton and event.is_pressed():
		device = event.device
	elif __is_any_event_for_keybaord(event, 'wasd'):
		device = WASD_DEVICE_ID
	elif __is_any_event_for_keybaord(event, 'arrows'):
		device = ARROWS_DEVICE_ID
	elif __is_any_event_for_keybaord(event, 'ijkl'):
		device = IJKL_DEVICE_ID
	elif __is_any_event_for_keybaord(event, 'numpad'):
		device = NUMPAD_DEVICE_ID
	else:
		return

	for player in _current_players:
		if player.device == device:
			return

	_current_players.append({
		device = device,
		color = null
	})

	if device >= 0:
		Input.start_joy_vibration(device, 1.0, 0.1, 0.25)

	emit_signal('players_changed', _current_players)
	print('[MultiInput] added new player %d for device=%d'%[
		_current_players.size()-1,
		device
	])


func __is_any_event_for_keybaord(event: InputEvent, id: String) -> bool:
	if !(event is InputEventKey):
		return false

	return (
		event.is_action_pressed('%s_up'%id) or 
		event.is_action_pressed('%s_left'%id) or 
		event.is_action_pressed('%s_down'%id) or
		event.is_action_pressed('%s_right'%id) or
		event.is_action_pressed('%s_hold'%id) or
		event.is_action_pressed('%s_action'%id) or
		event.is_action_pressed('%s_powerup'%id)
	)

func __is_action_pressed_for_device(event: InputEvent, device: int, action_suffix: String) -> bool:
	if device >= 0:
		return device == event.device && event.is_action_pressed('joy_' + action_suffix)
	elif device == WASD_DEVICE_ID:
		return event.is_action_pressed('wasd_' + action_suffix)
	elif device == ARROWS_DEVICE_ID:
		return event.is_action_pressed('arrows_' + action_suffix)
	elif device == IJKL_DEVICE_ID:
		return event.is_action_pressed('ijkl_' + action_suffix)
	elif device == NUMPAD_DEVICE_ID:
		return event.is_action_pressed('numpad_' + action_suffix)
	else:
		return false

func __is_action_released_for_device(event: InputEvent, device: int, action_suffix: String) -> bool:
	if device >= 0:
		return device == event.device && event.is_action_released('joy_' + action_suffix)
	elif device == WASD_DEVICE_ID:
		return event.is_action_released('wasd_' + action_suffix)
	elif device == ARROWS_DEVICE_ID:
		return event.is_action_released('arrows_' + action_suffix)
	elif device == IJKL_DEVICE_ID:
		return event.is_action_released('ijkl_' + action_suffix)
	elif device == NUMPAD_DEVICE_ID:
		return event.is_action_released('numpad_' + action_suffix)
	else:
		return false

func __get_move_vector_for_keyboard(id: String) -> Vector3:
	var x = Input.get_axis('%s_left'%id, '%s_right'%id)
	var z = Input.get_axis('%s_up'%id , '%s_down'%id)

	return Vector3(x, 0, z)
