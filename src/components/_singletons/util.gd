extends Node


var is_debug = OS.has_feature('editor')

var rng = RandomNumberGenerator.new()


func _ready():
	rng.randomize()


func format_count(count: int) -> String:
	var text = str(count)

	if count >= 1_000_000_000:
		text = 'a lot'
	elif count >= 1_000_000:
		text = '%s.%03d.%03d' % [
			floor(count / 1_000_000), floor((count % 1_000_000) / 1_000), count % 1_000
		]
	elif count >= 1_000:
		text = '%s.%03d' % [floor(count / 1_000), count % 1_000]

	return text


func format_time_to_minutes(seconds: int) -> String:
	return '%02d:%02d' % [floor(seconds / 60), seconds % 60]
