extends Spatial


var start_height
var existence_time = 0
var _type

func _ready():
	start_height = get_child(1).get_translation().y
	connect('body_entered', self, '_on_body_entered')


func set_type(new_type: String):
	_type = new_type


func get_type() -> String:
	return _type


func _on_body_entered(body: Node):
	EVENTS.emit_signal('power_up_touched', body, self)


func _physics_process(delta):
	existence_time += delta
	translation.y = start_height + (sin(existence_time * 2.5) + 1.0) * 0.4
