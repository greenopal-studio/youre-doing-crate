extends MeshInstance


var anim_counter = 0
var reference_y = 0
var material


func _ready():
	connect('visibility_changed', self, '_on_visibility_changed')
	reference_y = get_translation().y

	# unlink material to be able to re-color it:
	material = get_active_material(0).duplicate()
	set_surface_material(0, material)


func _on_visibility_changed():
	if not is_visible_in_tree():
		anim_counter = 0
		translation.y = 0


func _physics_process(delta):
	if is_visible_in_tree():
		translation.y = reference_y + sin((anim_counter + delta) * 4) * 0.25
		anim_counter += delta


func set_color_for_player(id: int):
	var color =  MULTI_INPUT.get_player_by_id(id).color
	material.albedo_color = color
	material.emission = color
