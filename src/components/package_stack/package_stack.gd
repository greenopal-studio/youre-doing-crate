extends StaticBody


onready var VARIANTS = {
	ID.PACKAGE_SIZE_PAPERBAG: $paperbag_stack,
	ID.PACKAGE_SIZE_PACKAGE: $package_stack,
	ID.PACKAGE_SIZE_CRATE: $crate_stack
}
onready var marker1 = $Marker

var looking_at_pos = 0
var current_variant = ID.PACKAGE_SIZE_PAPERBAG
var type = ID.COMPONENT_TYPE_PACKAGE_STACK


func _ready():
	pass # Replace with function body.


func set_variant(variant: String):
	VARIANTS[current_variant].hide()
	VARIANTS[variant].show()
	current_variant = variant


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 0:
		looking_at_pos += 1
		if looking_at_pos == 1:
			marker1.show()
			marker1.set_color_for_player(player_id)

func remove_selection(collision_shape_index: int):
	if collision_shape_index == 0:
		looking_at_pos -= 1
		if looking_at_pos == 0:
			marker1.hide()
