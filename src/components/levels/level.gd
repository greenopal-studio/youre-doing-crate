extends Spatial

class_name Level


const POWER_UP_TIME = 15
const MAX_POWER_UPS = 4

var AVAILABLE_ITEMS = DATA.ITEMS.keys()

# needs to be filled in the ready func of the level
var START_POS = [Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO]
var LEVEL_DESIGN = []
var LEVEL_TOP = 0
var LEVEL_LEFT = 0
var ORDER_AMOUNT = 4
var POWER_UP_SPAWN_POINTS = []
var POWER_UP_GROUP_NODE = null

var power_up_timer = Timer.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	power_up_timer.set_wait_time(POWER_UP_TIME)
	power_up_timer.connect('timeout', self, '_on_power_up_spawn')
	add_child(power_up_timer)
	power_up_timer.start()


func place_items_on_shelves(shelf_group_node: Spatial):
	var available_items = DATA.ITEMS.keys()
	var free_spaces = shelf_group_node.get_child_count() - available_items.size()
	for shelf in shelf_group_node.get_children():
		var item = ID.ITEM_EMPTY
		if (free_spaces == 0 or UTIL.rng.randf() > 0.2) and available_items.size() > 0:
			item = available_items.pop_at(UTIL.rng.randi() % available_items.size())
		else:
			free_spaces -= 1
		for item_no in 3:
			shelf.add_item(item)


func build_level_design():
	for line_no in LEVEL_DESIGN.size():
		for tile_no in LEVEL_DESIGN[line_no].size():
			var tile_key = LEVEL_DESIGN[line_no][tile_no]
			if not DATA.COMPONENTS.has(tile_key):
				continue
			
			__check_tile(tile_key, tile_no, line_no, tile_key)

			match tile_key.left(2):
				'F1', 'F2', 'F3', 'F4', 'F5', 'F6':
					START_POS[int(tile_key[1]) - 1] = Vector3(tile_no + LEVEL_TOP, 0, line_no + LEVEL_LEFT)


func __check_tile(tile_key: String, tile_no: int, line_no: int, source_key: String, parts: Dictionary = {}):
	var component = DATA.COMPONENTS[tile_key]
	if tile_key.length() > 1:
		var is_else = false
		for part in component.parts:
			var local_parts = parts.duplicate()
			for key in ['rotation_y', 'translate_x', 'translate_y', 'translate_z', 'variant']:
				if not key in part:
					continue
				
				if key in local_parts:
					local_parts[key] += part[key]
				else:
					local_parts[key] = part[key]
			if part.has('else'):
				if is_else:
					__check_tile(part.type, tile_no, line_no, source_key, local_parts)
				continue
			elif part.has('if_players'):
				if part.if_players > MULTI_INPUT.get_current_players_count():
					is_else = true
					continue

			__check_tile(part.type, tile_no, line_no, source_key, local_parts)
	else:
		__add_component(tile_key, tile_no, line_no, source_key, parts)


func __add_component(tile_key: String, x: int, z: int, source_key: String, part: Dictionary = {}):
	var component = DATA.COMPONENTS[tile_key]
	var new_component = component.obj.instance()
	var old_translation = new_component.get_translation()
	if part.has('rotation_y'):
		new_component.rotate_y(part.rotation_y)
		old_translation = old_translation.rotated(Vector3.UP, part.rotation_y)
	if part.has('translate_x'):
		old_translation.x += part.translate_x
	if part.has('translate_y'):
		old_translation.y += part.translate_y
	if part.has('translate_z'):
		old_translation.z += part.translate_z
	old_translation.x += LEVEL_TOP + 1 * x
	old_translation.z += LEVEL_LEFT + 1 * z
	new_component.set_translation(old_translation)
	get_node(component.group).add_child(new_component)
	if DATA.COMPONENTS[source_key].has('power_up_spawns') and tile_key == 'F':
		POWER_UP_SPAWN_POINTS.append(old_translation)
	if part.has('variant'):
		new_component.set_variant(part.variant)


func _on_power_up_spawn():
	if POWER_UP_GROUP_NODE == null or STATE.get_is_tutorial():
		return

	if POWER_UP_GROUP_NODE.get_child_count() < MAX_POWER_UPS:
		var new_power_up_key = DATA.POWER_UPS.keys()[UTIL.rng.randi() % DATA.POWER_UPS.size()]
		var new_power_up = DATA.POWER_UPS[new_power_up_key].obj.instance()
		var pos = POWER_UP_SPAWN_POINTS[UTIL.rng.randi() % POWER_UP_SPAWN_POINTS.size()]
		var is_free = false
		var while_limit = 0
		while not is_free and while_limit < 10:
			while_limit += 1
			is_free = true
			for power_up in POWER_UP_GROUP_NODE.get_children():
				if power_up.get_translation() == pos:
					pos = POWER_UP_SPAWN_POINTS[UTIL.rng.randi() % POWER_UP_SPAWN_POINTS.size()]
					is_free = false
		if while_limit == 10:
			return # no pos found
		
		new_power_up.set_translation(pos)
		new_power_up.set_type(new_power_up_key)
		POWER_UP_GROUP_NODE.add_child(new_power_up)
	power_up_timer = POWER_UP_TIME
