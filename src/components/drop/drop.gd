extends RigidBody


const DROP_ANMIM_DURATION = 0.15
const YEET_IMPULSE_STRENGTH = 24.0
const YEET_BOUNCE_DISTANCE = 0.2


onready var type = ID.COMPONENT_TYPE_DROP

onready var small_collision_shape = $SmallCollisionShape
onready var medium_collision_shape = $MediumCollisionShape
onready var large_collision_shape = $LargeCollisionShape
onready var anchor = $Anchor
onready var animation_player = $AnimationPlayer
onready var marker = $Marker


var item_id: String


func set_item_for_drop(_id: String, rot_y: float, target_pos: Vector3, drop_start_pos: Vector3):
	var collision_shape_name = __base_setup(_id, rot_y)

	global_translation = target_pos

	mode = MODE_STATIC

	var drop_animation = Animation.new()
	var y_track = drop_animation.add_track(Animation.TYPE_VALUE)
	drop_animation.track_set_path(y_track, "Anchor:global_translation")
	drop_animation.track_set_interpolation_type(y_track, Animation.INTERPOLATION_CUBIC)
	drop_animation.track_insert_key(y_track, 0.0, drop_start_pos)
	drop_animation.track_insert_key(y_track, DROP_ANMIM_DURATION, target_pos)
	var body_disable_track = drop_animation.add_track(Animation.TYPE_VALUE)
	drop_animation.track_set_path(body_disable_track, "%s:disabled"%collision_shape_name)
	drop_animation.value_track_set_update_mode(body_disable_track, Animation.UPDATE_DISCRETE)
	drop_animation.track_insert_key(body_disable_track, 0.0, true)
	drop_animation.track_insert_key(body_disable_track, DROP_ANMIM_DURATION * 2.0, false)
	animation_player.add_animation('Drop', drop_animation)
	animation_player.play('Drop')

func set_item_for_yeet(_id: String, rot_y: float, drop_start_pos: Vector3):
	var collision_shape_name = __base_setup(_id, rot_y)
	var collision_shape = get_node(collision_shape_name)
	collision_shape.disabled = true
	
	contact_monitor = true
	contacts_reported = 1
	continuous_cd = true
	connect('body_shape_entered', self, '_on_yeeted_body_entered', [], CONNECT_ONESHOT)
	
	global_translation = drop_start_pos
	apply_central_impulse(Vector3(0, 0.3, 1).rotated(Vector3.UP, rot_y) * YEET_IMPULSE_STRENGTH)
	
	yield(get_tree().create_timer(DROP_ANMIM_DURATION), "timeout")
	collision_shape.disabled = false
	
	
func _on_yeeted_body_entered(_body_rid, hit_body, body_shape_index, _local_shape_index):
	if 'type' in hit_body:
		match hit_body.type:
			ID.COMPONENT_TYPE_PACKAGING_STATION, ID.COMPONENT_TYPE_FRAGILE_STATION:
				var is_success = hit_body.add_stored_item(item_id, body_shape_index)
				if is_success:
					queue_free()
					return 

	linear_damp = -1
	mode = MODE_STATIC

	yield(get_tree(), "idle_frame") # to re-calculate position

	var target_translation = translation
	if translation.y > 0.1: # probably not ground:
		target_translation = translation - (linear_velocity * YEET_BOUNCE_DISTANCE)
	target_translation.y = 0.0

	var drop_animation = Animation.new()
	var t_y_track = drop_animation.add_track(Animation.TYPE_VALUE)
	drop_animation.track_set_path(t_y_track, ":translation")
	drop_animation.track_set_interpolation_type(t_y_track, Animation.INTERPOLATION_CUBIC)
	drop_animation.track_insert_key(t_y_track, 0.0, translation)
	drop_animation.track_insert_key(t_y_track, DROP_ANMIM_DURATION, target_translation)
	var rot_x_track = drop_animation.add_track(Animation.TYPE_VALUE)
	drop_animation.track_set_path(rot_x_track, ":rotation:x")
	drop_animation.track_set_interpolation_type(rot_x_track, Animation.INTERPOLATION_CUBIC)
	drop_animation.track_insert_key(rot_x_track, 0.0, rotation.x)
	drop_animation.track_insert_key(rot_x_track, DROP_ANMIM_DURATION, 0)
	var rot_z_track = drop_animation.add_track(Animation.TYPE_VALUE)
	drop_animation.track_set_path(rot_z_track, ":rotation:z")
	drop_animation.track_set_interpolation_type(rot_z_track, Animation.INTERPOLATION_CUBIC)
	drop_animation.track_insert_key(rot_z_track, 0.0, rotation.z)
	drop_animation.track_insert_key(rot_z_track, DROP_ANMIM_DURATION, 0)
	animation_player.add_animation('Drop', drop_animation)
	animation_player.play('Drop')


func __base_setup(_id: String, rot_y: float) -> String:
	item_id = _id

	marker.hide()

	var item
	var item_obj
	if item_id in DATA.ITEMS:
		item = DATA.ITEMS[item_id]
		item_obj = item.obj
	elif item_id in DATA.PACKAGE_SIZES:
		item = DATA.PACKAGE_SIZES[item_id]
		item_obj = item.parts_obj
	else:
		return ''
	
	var item_node = item_obj.instance()
	anchor.add_child(item_node)
	item_node.rotation.y = rot_y

	var collision_shape_name = ''
	if item_id in DATA.ITEMS:
		if item.size > DATA.PACKAGE_SIZES[ID.PACKAGE_SIZE_PACKAGE].space:
			collision_shape_name = 'LargeCollisionShape'
		elif item.size > DATA.PACKAGE_SIZES[ID.PACKAGE_SIZE_PAPERBAG].space:
			collision_shape_name = 'MediumCollisionShape'
		else:
			collision_shape_name = 'SmallCollisionShape'
	elif item_id in DATA.PACKAGE_SIZES:
		match item_id:
			ID.PACKAGE_SIZE_PAPERBAG:
				collision_shape_name = 'SmallCollisionShape'
			ID.PACKAGE_SIZE_PACKAGE:
				collision_shape_name = 'MediumCollisionShape'
			ID.PACKAGE_SIZE_CRATE:
				item_node.set_rotation(Vector3(0.0, 90.0 + rot_y, 0.0))
				collision_shape_name = 'LargeCollisionShape'

	if collision_shape_name == 'LargeCollisionShape':
		medium_collision_shape.queue_free()
		small_collision_shape.queue_free()
	elif collision_shape_name == 'MediumCollisionShape':
		large_collision_shape.queue_free()
		small_collision_shape.queue_free()
	elif collision_shape_name == 'SmallCollisionShape':
		large_collision_shape.queue_free()
		medium_collision_shape.queue_free()

	return collision_shape_name

func add_selection(_collision_shape_index: int, player_id: int):
	marker.show()
	marker.set_color_for_player(player_id)

func remove_selection(_collision_shape_index: int):
	marker.hide()
