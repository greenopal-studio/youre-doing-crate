extends Spatial


var timer_reference: Timer

onready var texture_progress = $'%TextureProgress'


func _ready():
	get_tree().connect('screen_resized', self, '_on_screen_resize')


func set_timer(timer: Timer):
	timer_reference = timer
	timer_reference.connect('timeout', self, '_on_timeout')


func set_color(color: Color):
	texture_progress.set_tint_progress(color)


func _on_timeout():
	texture_progress.set_value(0)
	hide()


func _on_screen_resize():
	$Viewport/PanelContainer/VBox/Title.set_text('')
	$Viewport/PanelContainer/VBox/Title.set_text('Wörking...')


func _physics_process(_delta):
	if not timer_reference.is_stopped():
		texture_progress.set_value((1 - (timer_reference.time_left / timer_reference.wait_time)) * 100)
