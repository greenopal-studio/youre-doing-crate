extends Control


func _ready():
	$'%StartButton'.connect(
		'pressed', self, '_on_start_pressed'
	)
	$'%TutorialButton'.connect(
		'pressed', self, '_on_tutorial_pressed'
	)
	$'%ControlsButton'.connect(
		'pressed', self, '_on_controls_pressed'
	)

func _input(event):
	if UTIL.is_debug:
		if event.is_action_pressed('debug_f1'):
			MULTI_INPUT.reset()
			MULTI_INPUT.add_player({
				device = MULTI_INPUT.WASD_DEVICE_ID,
				color = Color.white
			})
			MULTI_INPUT.add_player({
				device = 0,
				color = Color.black
			})
			STATE.set_level(ID.LEVEL_TUTORIAL)
			STATE.set_as_tutorial()
			SCENE.goto_scene('world')


func _on_start_pressed():
	STATE.set_level(ID.LEVEL_WAREHOUSE)
	SCENE.goto_scene('setup')

func _on_tutorial_pressed():
	STATE.set_level(ID.LEVEL_TUTORIAL)
	STATE.set_as_tutorial()
	SCENE.goto_scene('setup')

func _on_controls_pressed():
	SCENE.goto_scene('controls')
