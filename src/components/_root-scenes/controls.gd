extends Control


func _ready():
	$'%BackButton'.connect('pressed', self, '_on_back_pressed')

	ANALYTICS.event('show controls')


func _on_back_pressed():
	SCENE.goto_scene('menu')
