extends Spatial


var player_scene = preload('res://components/player/player.tscn')


onready var round_timer = $RoundTimer
onready var player_anchor = $PlayerAnchor


func _ready():
	round_timer.connect('timeout', self, '_on_level_finished')
	
	var level_obj = DATA.LEVELS[STATE.get_level()].obj.instance()
	add_child(level_obj)
	
	STATE.generate_order_list(level_obj.ORDER_AMOUNT, level_obj.AVAILABLE_ITEMS)
	for i in STATE.MAX_ORDERS_ON_SCREEN:
		STATE.add_new_order_to_screen()
	
	for id in  MULTI_INPUT.get_current_players().size():
		var new_player = player_scene.instance()
		player_anchor.add_child(new_player)
		new_player.set_data(id)
		new_player.set_translation(level_obj.START_POS[id])
	
	round_timer.start()
	EVENTS.emit_signal('send_round_timer', round_timer)


func _on_level_finished():
	SCENE.goto_scene('win')
