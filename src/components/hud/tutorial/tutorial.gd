extends PanelContainer


const TEXTS = [
"""
Welcome to the warehouse! As it's your first shift, we'll explain some things.
At the top you can see the orders we've got to deliver. Let's walk over to the shelf and pick up the rubber ducky.
""",
"""
Great, that's the first step done. Go you!
Now that you're carrying an item, you can use the same key/button to drop it again.
Items can be dropped onto a packaging station or onto the floor, and also picked up again from both.
They can also be yeeted across the warehouse, I've heard.
""",
"""
Now we'll need some packaging to wrap up the order - or a handling unit, as the pros call it.
At the top of the warehouse we have some stacks of material. The order indicates what size of packaging we need.
Pick it up and place it onto the packaging station as well.
Then you can package the order by pressing and holding the action key.
""",
"""
Alright, the order is ready for delivery.
Once the order is packaged, it gets a green check in the order list at the top. So don't forget to deliver the packages.
Pick it up and carry it over to the train. Then drop it into the train wagon.
If the package has been delivered, it will vanish from the order list, so don't be surprised.
""",
"""
Some items are fragile and need to be handled with extra care.
Any order containing a fragile item will be marked with a red \"FRAGILE\" sticker in the order overview.
These orders need to be packaged at the bigger station, which includes a dispenser for padding material.
Let's package a controller for the next order, which needs to be packaged this way.
""",
"""
Certain orders are placed by our VIP \"Mövenlieferung Plus Plus Special\" customers.
Such orders are marked with a yellow \"PRIORITY\" sticker in the order overview.
These get priority delivery by courier truck. As they are time-sensitive, the truck is only waiting a limited time.
After that you'll need for it to complete the deliveries before it is available again.
Apart from this, they are packaged like normal. Some orders can even be both fragile and priority.
""",
"""
As a reward and to make your job easier, we may dispense power-ups in the warehouse from time to time.
After you collected one, it is shown in the bottom bar and can be used at any time. However their effects only last a limited time.
There's different kinds of power-ups: Ghosting, dashing, juggling and a piggy bank.
""",
]


onready var text_label = $'%TextLabel'
onready var base_controls_container = $'%BaseControlsContainer'
onready var yeet_controls_container = $'%YeetControlsContainer'
onready var powerup_controls_container = $'%PowerupControlsContainer'
onready var steps_container = $'%StepsContainer'
onready var steps_checkboxes = [
	$'%Step1CheckBox',
	$'%Step2CheckBox',
	$'%Step3CheckBox',
	$'%Step4CheckBox',
	$'%Step5CheckBox',
	$'%Step6CheckBox',
	$'%Step7CheckBox',
]
onready var exit_button = $'%ExitButton'


var current_step = 0


func _ready():
	if !STATE.get_is_tutorial():
		queue_free()

	text_label.set_text(TEXTS[0])
	base_controls_container.show()
	
	steps_container.hide()
	yeet_controls_container.hide()
	powerup_controls_container.hide()
	exit_button.hide()

	exit_button.connect('pressed', self, '_on_exit_pressed')

	EVENTS.connect('tutorial_picked_item_from_shelf', self, '_on_tutorial_picked_item_from_shelf')
	EVENTS.connect('tutorial_dropped_item_on_station', self, '_on_tutorial_dropped_item_on_station')
	EVENTS.connect('order_removed', self, '_on_order_removed')
	EVENTS.connect('tutorial_delivered_normal', self, '_on_tutorial_delivered_normal')
	EVENTS.connect('tutorial_delivered_fragile', self, '_on_tutorial_delivered_fragile')
	EVENTS.connect('tutorial_delivered_priority', self, '_on_tutorial_delivered_priority')

	ANALYTICS.event('tutorial started')


func _on_advance_step():
	steps_checkboxes[current_step].pressed = true

	current_step += 1

	text_label.set_text(TEXTS[current_step])

	match current_step:
		1:
			steps_container.show()
			base_controls_container.hide()
			yeet_controls_container.show()
		2:
			pass # keep controls visible for interact
		3:
			yeet_controls_container.hide()
		6:
			powerup_controls_container.show()
			exit_button.show()

	ANALYTICS.event('tutorial step', {step=current_step})


func _on_tutorial_picked_item_from_shelf():
	if current_step == 0:
		_on_advance_step()

func _on_tutorial_dropped_item_on_station():
	if current_step == 1:
		_on_advance_step()

func _on_order_removed(_a, _b):
	if current_step == 2:
		_on_advance_step()

func _on_tutorial_delivered_normal():
	if current_step == 3:
		_on_advance_step()

func _on_tutorial_delivered_fragile():
	if current_step == 4:
		_on_advance_step()

func _on_tutorial_delivered_priority():
	if current_step == 5:
		_on_advance_step()


func _on_exit_pressed():
	ANALYTICS.event('tutorial completed')
	STATE.reset_level_data()
	SCENE.goto_scene('menu')
