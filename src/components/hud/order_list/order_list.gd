extends HBoxContainer


var order_scene = load('res://components/hud/order/order.tscn')


func _ready():
	EVENTS.connect('new_order_appeared', self, '_on_new_order_appeared')
	EVENTS.connect('order_removed', self, '_on_order_removed')


func _on_new_order_appeared(order_id: int):
	var new_order_scene = order_scene.instance()
	add_child(new_order_scene)
	new_order_scene.set_order(order_id)


func _on_order_removed(order_screen_index: int, _order_id: int):
	get_child(order_screen_index).queue_free()
