extends Label


var round_timer


# Called when the node enters the scene tree for the first time.
func _ready():
	EVENTS.connect('send_round_timer', self, '_on_get_round_timer')


func _on_get_round_timer(round_timer_obj: Timer):
	round_timer = round_timer_obj


func _process(_delta):
	if round_timer:
		set_text(UTIL.format_time_to_minutes(round_timer.get_time_left()))
