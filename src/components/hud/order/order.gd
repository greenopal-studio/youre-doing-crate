extends PanelContainer


onready var item_nodes = [$'%Item1',$'%Item2',$'%Item3',$'%Item4']

var order = -1


func _ready():
	EVENTS.connect('order_finished', self, '_on_order_finished')


func set_order(order_id):
	order = order_id
	var order_data = STATE.get_order(order_id)
	if order_data.is_fragile:
		$'%FragileLabel'.show()
	if order_data.is_priority:
		$'%PriorityLabel'.show()
	for item_no in order_data.items.size():
		var item_id = order_data.items[item_no]
		var item_data = DATA.ITEMS[item_id]
		item_nodes[item_no].set_texture(item_data.icon)
		item_nodes[item_no].show()
	
	$'%OrderIdentificationNumber'.set_text('#%016d' % [order_data.order_identification_number])
	$'%PackageSize'.set_texture(DATA.PACKAGE_SIZES[order_data.package_size].icon)


func _on_order_finished(order_id: int):
	if order_id == order:
		$'%IsFinished'.show()
