extends StaticBody


onready var marker1 = $Marker

var looking_at_pos = 0
var type = ID.COMPONENT_TYPE_WRAPPING_STATION


func _ready():
	pass


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 1:
		looking_at_pos += 1
		if looking_at_pos == 1:
			marker1.show()
			marker1.set_color_for_player(player_id)

func remove_selection(collision_shape_index: int):
	if collision_shape_index == 1:
		looking_at_pos -= 1
		if looking_at_pos == 0:
			marker1.hide()
