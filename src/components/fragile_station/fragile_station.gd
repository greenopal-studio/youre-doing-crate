extends StaticBody


onready var work_timer = $WorkTimer
onready var marker1 = $Marker
onready var pos1 = $Position3D
onready var pos2 = $Position3D2
onready var pos3 = $Position3D3
onready var pos4 = $Position3D4
onready var package_pos = $PackagePos
onready var finished_package_pos = $FinishedPackagePos
onready var error_node = $Error

var looking_at_pos = 0
var current_order_id = -1
var items_stored = []
var items_put_order = []
var stored_package_material = null
var package_finished = false
var type = ID.COMPONENT_TYPE_FRAGILE_STATION


func _ready():
	work_timer.connect('timeout', self, '_on_work_finished')
	$Progress.set_timer(work_timer)


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 2 or collision_shape_index == 3:
		looking_at_pos += 1
		if looking_at_pos == 1:
			marker1.show()
			marker1.set_color_for_player(player_id)


func remove_selection(collision_shape_index: int):
	if collision_shape_index == 2 or collision_shape_index == 3:
		looking_at_pos -= 1
		if looking_at_pos == 0:
			marker1.hide()


func add_stored_item(item_id: String, collision_shape_index: int) -> bool:
	if not collision_shape_index in [2, 3]:
		return false
	
	if item_id in DATA.PACKAGE_SIZES:
		if stored_package_material != null:
			return false
		stored_package_material = item_id
		var package_obj = DATA.PACKAGE_SIZES[item_id].parts_obj.instance()
		package_obj.set_translation(package_pos.get_translation())
		package_obj.set_scale(Vector3(0.25, 0.25, 0.25))
		$Packages.add_child(package_obj)
	else:
		if items_stored.size() > 4:
			return false
		items_stored.append(item_id)
		current_order_id = STATE.get_active_order_by_items(items_stored, true)
		var item_obj = DATA.ITEMS[item_id].obj.instance()
		item_obj.set_scale(Vector3(0.25, 0.25, 0.25))
		match items_stored.size():
			1:
				item_obj.set_translation(pos1.get_translation())
			2:
				item_obj.set_translation(pos2.get_translation())
			3:
				item_obj.set_translation(pos3.get_translation())
			4:
				item_obj.set_translation(pos4.get_translation())
		item_obj.rotate_y(UTIL.rng.randf() * PI * 2)
		$Items.add_child(item_obj)
	items_put_order.append(item_id)
	return true


func get_stored_item() -> String:
	if package_finished:
		package_finished = false
		$Packages.get_child(0).queue_free()
		return ID.ITEM_ORDER
	if items_put_order.size() == 0:
		return ID.ITEM_EMPTY
	var last_item = items_put_order.pop_back()
	if last_item in DATA.PACKAGE_SIZES:
		stored_package_material = null
		$Packages.get_child(0).queue_free()
	else:
		items_stored.pop_back()
		$Items.get_child($Items.get_child_count() - 1).queue_free()
	return last_item


func get_order_id() -> int:
	return current_order_id


func start_work(color: Color, player_id: int) -> bool:
	if current_order_id != -1 and (work_timer.is_stopped() or work_timer.is_paused()) and stored_package_material != null:
		if (STATE.is_order_being_worked_on_by(current_order_id, player_id) or
			STATE.is_order_available(current_order_id)):
			if work_timer.is_paused():
				work_timer.set_paused(false)
			else:
				STATE.start_work_on_order(current_order_id, player_id)
				work_timer.start()
				$Progress.set_color(color)
				$Progress.show()
			return true
	if current_order_id == -1:
		error_node.set_error_message('This equals no current fragile order', true)
	if stored_package_material == null:
		error_node.set_error_message('There is no package material', true)
	if not STATE.is_order_available(current_order_id):
		if STATE.is_order_finished(current_order_id):
			error_node.set_error_message('The order is already finished', true)
		elif not STATE.is_order_being_worked_on_by(current_order_id, player_id):
			error_node.set_error_message('The order is already being worked on', true)
	return false


func pause_work():
	if not work_timer.is_stopped():
		work_timer.set_paused(true)


func _on_work_finished():
	for child in $Items.get_children():
		child.queue_free()
	for child in $Packages.get_children():
		child.queue_free()
	items_stored = []
	items_put_order = []
	
	var order_data = STATE.get_order(current_order_id)
	var package_obj
	if order_data.is_priority:
		package_obj = DATA.PACKAGE_SIZES[stored_package_material].packed_fragile_priority_obj.instance()
	else:
		package_obj = DATA.PACKAGE_SIZES[stored_package_material].packed_fragile_obj.instance()
	package_obj.set_translation(finished_package_pos.get_translation())
	package_obj.set_scale(Vector3(0.25, 0.25, 0.25))
	package_obj.rotate_y(UTIL.rng.randf() * PI * 2)
	STATE.set_order_as_finished(current_order_id)
	$Packages.add_child(package_obj)
	stored_package_material = null
	package_finished = true
